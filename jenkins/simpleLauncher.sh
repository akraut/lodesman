#!/bin/bash

# for local debugging
if [ -z ${LCG_VERSION+x} ]; then LCG_VERSION="dev3"; fi
if [ -z ${COMPILER+x} ]; then COMPILER="gcc8binutils"; fi
if [ -z ${BUILD_TYPE+x} ]; then BUILD_TYPE="opt"; fi
if [ -z ${LCG_INSTALL_PREFIX+x} ]; then LCG_INSTALL_PREFIX="/cvmfs/sft.cern.ch/lcg/releases"; fi
if [ -z ${VERSION_MAIN+x} ]; then VERSION_MAIN="master"; fi
if [ -z ${TARGET+x} ]; then TARGET="all"; fi
if [ -z ${LCG_EXTRA_OPTIONS+x} ]; then LCG_EXTRA_OPTIONS="-DLCG_TARBALL_INSTALL=OFF;-DLCG_SOURCE_INSTALL=OFF"; fi
if [ -z ${OS+x} ]; then OS="centos7"; fi
if [ -z ${BUILD_MODE+x} ]; then BUILD_MODE="nightly"; fi
if [ -z ${WORKSPACE+x} ]; then WORKSPACE="/workspace"; fi
if [ -z ${CTEST_TRACK+x} ]; then CTEST_TRACK="nightly"; fi

INGRESS="188.184.94.235"
PORT="80"
ENDPOINT="build"
TIMESTAMP="$(date +"%s")"

JOBNAME="$(curl \
  --header "Content-Type: application/json" \
  --request POST \
  --data '{"name":"buildsim","timestamp":"'"${TIMESTAMP}"'","lcg_install_prefix":"'"${LCG_INSTALL_PREFIX}"'","version_main":"'"${VERSION_MAIN}"'","target":"'"${TARGET}"'","lcg_extra_options":"'"${LCG_EXTRA_OPTIONS}"'","lcg_version":"'"${LCG_VERSION}"'","compiler":"'"${COMPILER}"'","build_type":"'"${BUILD_TYPE}"'","os":"'"${OS}"'","build_mode":"'"${BUILD_MODE}"'","workspace":"'"${WORKSPACE}"'","ctest_track":"'"${CTEST_TRACK}"'"}' \
  "http://${INGRESS}:${PORT}/${ENDPOINT}")"

echo "Queued job $JOBNAME"

for i in {1..14}
do
   curl "http://${INGRESS}:${PORT}/status/buildsim-${LCG_VERSION}-${COMPILER}-${BUILD_TYPE}-${TIMESTAMP}/"
   sleep 5
done

echo "Build job logs:"
curl "http://${INGRESS}:${PORT}/logs/buildsim-${LCG_VERSION}-${COMPILER}-${BUILD_TYPE}-${TIMESTAMP}/"

exit 0
